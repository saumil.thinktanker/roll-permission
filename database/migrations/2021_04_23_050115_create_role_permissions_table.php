<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_permissions', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->integer('role_id')->nullable()->comment('Role Id');
           $table->integer('permission_id')->nullable()->comment('Acl permission Id');
           $table->integer('created_by')->nullable()->comment('Created by');
           $table->integer('updated_by')->nullable()->comment('Updated by');
           $table->timestamps();        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permissions');
    }
}
